<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="US-ASCII"%>

<%

%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>

<form action="/register" method="POST" enctype="multipart/form-data">
    <input type="text" name="firstName"  placeholder="first name"  autocomplete="off" required>
    <input type="text"  name="lastName"  placeholder="last name" autocomplete="off" required>
    <input type="file"  name="image" accept=".png, .jpg, .jpeg">
    <button type="submit">Register</button>
</form>
</body>
</html>

