package dataAccess;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.*;
import java.lang.*;
public class UserRepository {
    private static UserRepository userRepository;
    private final ComboPooledDataSource dataSource;
    private Connection connection;
    public UserRepository(){
        System.out.println("start");
        this.connection = MainRepository.getInstance().getConnection();
        this.dataSource = MainRepository.getInstance().getDataSource();
    }

    public static UserRepository getInstance(){
        if(userRepository == null)
            userRepository = new UserRepository();
        return userRepository;
    }

    public void addUser (String firstName, String lastName, InputStream img)
            throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "insert into User (user_id, user_first_name, user_last_name, image) " +
                        "values (?, ?, ?, ?);");

        String id = Integer.toString(getUserNumbers()  + 1);
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, firstName);
        preparedStatement.setString(3, lastName);
        preparedStatement.setBlob(4, img);
        preparedStatement.executeUpdate();
        preparedStatement.close();

    }

    public int getUserNumbers () throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("select count(*) as count from user;");
        result.next();
        int number = result.getInt("count");
        statement.close();
        return number;
    }

}
