package dataAccess;

import java.sql.Connection;
import java.sql.SQLException;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.ResultSet;
import java.sql.Statement;

public class MainRepository {

    private ComboPooledDataSource dataSource;
    private Connection connection;
    private final String database = "register";
    private static MainRepository instance;

    private MainRepository() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/register?autoReconnect=true&useSSL=false");
        dataSource.setUser("root");
        dataSource.setPassword("13785400");

        dataSource.setInitialPoolSize(5);
        dataSource.setMinPoolSize(5);
        dataSource.setAcquireIncrement(5);
        dataSource.setMaxPoolSize(20);
        dataSource.setMaxStatements(100);
        try {
            connection = dataSource.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void makeDatabaseReady () {
        createDatabase();
        uesDatabase();
        createUserTable();

    }
    public static MainRepository getInstance() {
        if (instance == null)
            instance = new MainRepository();
        return instance;
    }

    public ComboPooledDataSource getDataSource() {
        return dataSource;
    }

    public Connection getConnection() {
        return connection;
    }
    private void createDatabase () {
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select schema_name from information_schema.schemata where schema_name = '" + database + "';");
            if (!result.next()) {
                Statement creator = connection.createStatement();
                creator.executeUpdate("create database loghmeh;");
                creator.close();
            }
            statement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void uesDatabase () {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("use " + database);
            statement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private boolean tableExists (String tableName) {
        boolean exists = false;
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(
                    "select * from information_schema.tables " +
                            "where table_schema = '" + database + "' and table_name = '" + tableName + "';");
            if (result.next())
                exists = true;
            statement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return exists;
    }

    private void createUserTable () {
        if (tableExists("user"))
            return;
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(
                    "CREATE TABLE User (\n" +
                            "        user_id             VARCHAR(255) PRIMARY KEY,\n" +
                            "        user_first_name             VARCHAR(255) NOT NULL,\n" +
                            "        user_last_name            VARCHAR (255) NOT NULL,\n" +
                            "        image                   LONGBLOB NOT NULL,\n" +
                            "    );\n");
            statement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
