package domain;

import dataAccess.UserRepository;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;

public class UserManager {

    private static UserManager userManager;

    public static UserManager getInstance(){
        if(userManager == null)
            userManager = new UserManager();
        return userManager;
    }

    public void insertUser(String firstName, String lastName, InputStream image) throws SQLException, FileNotFoundException {
        UserRepository.getInstance().addUser(firstName, lastName, image);
        System.out.println("added successfully");
    }



}
