package presentation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import javax.servlet.http.Part;
import domain.UserManager;
@WebServlet("/register")
@MultipartConfig(maxFileSize = 161772150)
public class ServletUser extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            Part image = request.getPart("image");
            InputStream image_inStream = image.getInputStream();
            UserManager.getInstance().insertUser(request.getParameter("firstName"), request.getParameter("lastName"), image_inStream);
        } catch (SQLException | ServletException throwables) {
            System.out.println(throwables.getMessage());
        }
        RequestDispatcher rd = request.getRequestDispatcher("/successfullRegister.jsp");
        rd.forward(request, response);
    }
}

